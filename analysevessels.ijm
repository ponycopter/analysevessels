//ImageJ macro for guided vessel size analysis from xylem cross section images
//tested with ImageJ 1.50i and FIJI 1.49k
//macro output: (1) a copy of the edited image (after manual correction of vessel outlines etc.), (2) a vessel mask, (3) vessel outlines, and (4) a table including analysed xylem area and individual vessel measurements
//adjust vessel area range (µm²) to be analysed in lines 7 and 8.
//full script and documentation: https://bitbucket.org/ponycopter/analysevessels
//written in 2017 by Markus Nolf (m.nolf@westersydney.edu.au).

minarea = 50; //µm²
maxarea = 3000; //µm²


img = File.openDialog("Open image file");
open(img);
print("Opening file:");
print(img);
filepath = File.getParent(img);
filename = getTitle();
savefilename = File.nameWithoutExtension;

run("Set Measurements...", "area fit feret's redirect=None decimal=9");
run("Clear Results");

run("8-bit");
run("Brightness/Contrast...");
waitForUser("Adjust Brightness/Contrast. Then Apply");
setTool("line");
waitForUser("Redraw scale bar (if available). Hold Shift to draw a horizontal line.");

run("Set Scale...", "unit=µm");

run("Set Scale...");
run("Original Scale");


setForegroundColor(255, 255, 255);
setBackgroundColor(0, 0, 0);

setTool("freehand");
waitForUser("Draw outline of analysed xylem area.");
roiManager("Reset");
roiManager("Add");
run("Measure");
setResult("Comment",0,"Xylem area here, vessel areas below");
updateResults();

run("Threshold...");
setThreshold(230, 255);
setTool("Paintbrush Tool"); 
waitForUser("Adjust Threshold to select vessel lumens. \nThen correct damaged vessels using pencil tool.\n Notes: \n . vessel outlines are sufficient. \n . double-click the pencil or brush tool to adjust its radius. \n . draw = white\n . [Alt]+draw = black");

savefile = filepath+"/"+savefilename+" edited.tif";
saveAs("Tiff", savefile);
rename(filename);

roiManager("Select", 0);
run("Analyze Particles...", "size="+minarea+"-"+maxarea+" circularity=0.50-1.00 show=Outlines display exclude include");
selectWindow("Drawing of "+filename);
savefile = filepath+"/"+savefilename+" outlines.tif";
saveAs("Tiff", savefile);

selectWindow("Results");
saveresults = filepath+"/"+savefilename+" results.txt";
saveAs("Results", saveresults);


selectWindow(filename);
roiManager("Select", 0);setResult("Comment",1,"Xylem area above, vessel areas below");

run("Analyze Particles...", "size="+minarea+"-"+maxarea+" circularity=0.50-1.00 show=Masks exclude include");
selectWindow("Mask of "+filename);
savefile = filepath+"/"+savefilename+" mask.tif";
saveAs("Tiff", savefile);

waitForUser("Ready to close? Press [Esc] to cancel.");
run("Close All");

print(" ");
