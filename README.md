# README #

ImageJ macro for guided vessel size analysis from xylem cross section images (typically, sections imaged using light microscopy).
Adjust vessel size filter (minimum and maximum vessel area) in lines 8 and 9 of the macro. 

Macro output: 

* (1) a copy of the edited image (after manual correction of vessel outlines etc.), 
* (2) a vessel mask (image), 
* (3) vessel outlines (image), and 
* (4) a table including analysed xylem area and individual vessel measurements (tab-delimited text file)

R script:
* Calculates theoretical conductivity based on hydraulically weighted vessel diameters (KtDh) from Macro output results table. 

Version 1.1 (2017-11-10)



Written in 2017 by Markus Nolf (m.nolf@westersydney.edu.au).
