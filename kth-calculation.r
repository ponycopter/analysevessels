data <- read.table("C:\\[Path]\\Image1 results.txt", sep="\t", header=T)
head(data) #check that the file was imported correctly

totalaream2 <- data[1,"Area"]/1000000^2 #first line of the results table contains the selected xylem-crosssectional area
vessels <- data[2:nrow(data),] #all other lines are vessel/particle measurements

vessels$Diameter <- sqrt(vessels$Area/pi)*2 #calculate vessel diameters (um) based on circular shape
vessels$DiameterM <- vessels$Diameter/1000000 #convert from um to m
head(vessels)

vessels$Ktheoretical <- (pi*(vessels$DiameterM)^4)/(128*1.002*10^-9)*1000 #theoretical K [m4 MPa-1 s-1]
#see Lewis & Boose 1995, Brodersen et al. 2013, Choat et al. 2016, Nolf et al. 2017 for papers using this formula
head(vessels)

sum(vessels$Ktheoretical)
sumKtDh <- sum(vessels$Ktheoretical)/totalaream2 #theoretical K, hydraulically weighted [m4 MPa-1 s-1]
sumKtDh

paste(round(sumKtDh,3)," m4 MPa-1 s-1", sep="")
